**Nidhogg**

Fight with your opponents to advance in the next room. You can jump, throw him the sword or simply attack him. The first player who reaches the goal will win the match. Beware, your opponents will fight back!

_Commands Player1:_  
A -> Move Left  
D -> Move Right  
W -> Jump  
X -> Change Guard  
Z -> Attack  
Spacebar -> Throw Sword  

_Commands Player2:_  
J -> Move Left  
L -> Move Right  
I-> Jump  
N -> Change Guard  
P -> Attack  
M -> Throw Sword  

_Credits:_  
Amaducci Giada  
Fabris Anna  
Lega Davide  
Lucchi Riccardo  

This game needs Java 8 and JavaFX 8 to run.